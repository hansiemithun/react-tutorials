import React, {Component} from 'react';

const Child = (props) => {
  return (
      <h1>I am child of Parent: {props.name}</h1>
  )
};

const Parent = (props) => {
    return (
        <div>
            <h1>Reliance</h1><br/>

            <h2>Owner Name: {props.name}</h2><br/><br/>

            Son1: <input placeholder="Son 1 Name" type="text" value={props.son1} onChange={props.changeSonDetails}/><br/>
            Son2: <input placeholder="Son 2 Name" type="text" value={props.son2} onChange={props.changeSonDetails}/><br/><br/>

            <button onClick={props.anotherOwner}>Change Owner</button>

            <Child {...props}></Child>
        </div>
    )
};

class App extends Component {
    state = {
        title: 'Hello World!',
        user: 'Developer',
        name: 'Dheerubhai Ambani',
        son1: 'Mukesh Ambani',
        son2: 'Anil Ambani'
    };

    anotherOwner = (newOwner) => {
        this.setState({name: newOwner});
    };

    changeSonDetails = (newDetails) => {
        this.setState({son1: newDetails});
    };

    render() {
        return (
            <div className="App">
                <Parent anotherOwner={this.anotherOwner.bind(this, 'Mithun')} name={this.state.name} son1={this.state.son1} son2={this.state.son2} changeSonDetails={this.changeSonDetails}/> <br/>
                <b>Current Owner:</b> {this.state.name}
            </div>
        );
    }
}

export default App;
